(*********************************************************************************)
(*                OCaml-CSS                                                      *)
(*                                                                               *)
(*    Copyright (C) 2023 INRIA All rights reserved.                              *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

open Css

let init_reporter () =
  Fmt_tty.setup_std_outputs ();
  Logs.set_level (Some Logs.Debug);
  Logs.set_reporter (Logs_fmt.reporter ())

let () = init_reporter ()

module Imap = Map.Make(Int)

let test_parser ?(consume=Angstrom.Consume.All) parser str =
  let ctx = Css.T.string_ctx str in
  let parser = Vp.handle_end parser ctx in
  match Angstrom.parse_string ~consume parser str with
  | Ok v -> v
  | Error msg -> failwith msg

module To_test = struct
    let ident ?consume str = fst (test_parser ?consume Vp.ident str)
    let string = test_parser Vp.string
    let integer = test_parser U.integer
    let url = test_parser Vp.url
    let number = test_parser Vp.number
    let length = test_parser Vp.length
    let dimension = test_parser Vp.dimension
    let color ?consume str = test_parser ?consume Vp.color str
    let axis_position ?consume str = test_parser ?consume Vp.axis_position str
end

let ident_testable = Alcotest.string
let str_testable = Alcotest.testable T.pp_str (=)
let iri_testable = Alcotest.testable Iri.pp Iri.equal
let length_testable = Alcotest.(pair (float 0.01) (testable T.pp_length_unit (=)))
let dim_testable = Alcotest.(pair (float 0.01) (testable T.pp_dim_unit (=)))
let color_testable = Alcotest.testable T.pp_color (=)
let axis_position_testable = Alcotest.testable T.pp_axis_position (=)

(* The tests *)

let test_ident exp str () =
  Alcotest.(check ident_testable) "" exp (To_test.ident str)

let fail_ident str msg () =
  let f () = ignore(To_test.ident str) in
  let msg = Printf.sprintf "ident: %s" msg in
  Alcotest.(check_raises "" (Failure msg) f)

let test_string exp str () =
  Alcotest.(check str_testable) "" exp (To_test.string str)

exception E

let fail_string e str () =
  let f () =
    try ignore (To_test.string str)
    with T.(Error (Parse_error (_,e2))) when e2 = e -> raise E
    | _ -> ()
  in
  Alcotest.(check_raises "" E f)

let test_url exp str () =
  Alcotest.(check iri_testable "" exp (To_test.url str))

let fail_url str () =
  let f () =
    try ignore (To_test.url str)
    with T.(Error (Parse_error (_, Invalid_iri _))) -> raise E
  in
  Alcotest.(check_raises "" E) f

let test_number exp str () =
  Alcotest.(check (float 0.01)) "" exp (To_test.number str)

let test_length exp str () =
  Alcotest.(check length_testable) "" exp (To_test.length str)

let fail_length str () =
  let f () =
    try ignore (To_test.length str)
    with _ -> raise E
  in
  Alcotest.(check_raises "" E) f

let test_dimension exp str () =
  Alcotest.(check dim_testable) "" exp (To_test.dimension str)

let fail_dimension str () =
  let f () =
    try ignore (To_test.dimension str)
    with _ -> raise E
  in
  Alcotest.(check_raises "" E) f

let test_color exp str () =
  Alcotest.(check color_testable) "" exp (To_test.color str)

let fail_color str () =
  let f () =
    try ignore (To_test.color str)
    with T.(Error _) -> raise E
  in
  Alcotest.(check_raises "" E) f

let test_axis_position exp str () =
  Alcotest.(check axis_position_testable) "" exp (To_test.axis_position str)

let fail_axis_position str () =
  let f () =
    try ignore (To_test.axis_position str)
    with T.(Error _) -> raise E
  in
  Alcotest.(check_raises "" E) f

(* Run it *)
let go () =
  let open Alcotest in
  run "Utils" [
    "Idents", [
      test_case "simple" `Quick (test_ident "coucou" "coucou");
      test_case "hyphen" `Quick (test_ident "-coucou" "-coucou");
      test_case "double-hyphen" `Quick (test_ident "--coucou" "--coucou") ;
      test_case "digit" `Quick (fail_ident "0toto" "ident cannot start with <digit>") ;
      test_case "hyphen-digit" `Quick (fail_ident "-0" "ident cannot start with -<digit>") ;
      test_case "escaped-digit" `Quick (fail_ident "\\30toto" "ident cannot start with <digit>") ;
      test_case "hyphen-escaped-digit" `Quick (fail_ident "-\\30" "ident cannot start with -<digit>") ;
      test_case "escaped-hyphen" `Quick (test_ident "-toto" "\\2dtoto") ;
      test_case "escaped-chars" `Quick (test_ident "toto©" "toto\\a9") ;

    ];
    "Strings", [
      test_case "quoted" `Quick (test_string T.{ s = "coucou"; quoted = true } "'coucou'");
      test_case "double-quoted" `Quick (test_string T.{ s = "coucou"; quoted = true } "\"coucou\"");
      test_case "escaping" `Quick (test_string T.{ s = "\t\r\n\\zouzou"; quoted = true } "'\\t\\r\\n\\\\zouzou'" );
      test_case "escaping utf8" `Quick (test_string T.{ s = "\"©toto"; quoted = true } "'\\22\\a9toto'");
      test_case "unterminated" `Quick (fail_string Unterminated_string "'coucou") ;
      test_case "string cut by escaped newlines" `Quick (test_string T.{ s = "tototututata"; quoted = true } "'toto\\\ntutu\\\ntata'");
    ];
    (let i = "https://foo.bar" in
     "url()", [
       test_case "simple" `Quick (test_url (Iri.of_string i) (Printf.sprintf "url(%s)" i));
       test_case "simple with spaces" `Quick (test_url (Iri.of_string i) (Printf.sprintf " url(%s) " i));
       test_case "quoted" `Quick (test_url (Iri.of_string i) (Printf.sprintf "url(\'%s\')" i));
       test_case "double-quoted with spaces" `Quick (test_url (Iri.of_string i) (Printf.sprintf " url( \"%s\" )" i));
     ]
    ) ;
    "numbers", [
      test_case "regular" `Quick (test_number 12.34 "12.34") ;
      test_case "negative" `Quick (test_number (-12.34) "-12.34") ;
      test_case "frac only" `Quick (test_number 0.34 ".34") ;
      test_case "negative frac only" `Quick (test_number (-0.34) "-.34") ;
      test_case "integer" `Quick (test_number 12. "12") ;
      test_case "negative integer" `Quick (test_number (-12.) "-12") ;
    ] ;
    "lengths", [
      test_case "zero" `Quick (test_length (0.,`px) "0") ;
      test_case "1cm" `Quick (test_length (1.,`cm) "1cm") ;
      test_case "-2px" `Quick (test_length (-2.,`px) "-2px") ;
      test_case "fail -2 px" `Quick (fail_length "-2 px") ;
    ] ;
    "dimensions", [
      test_case "zero" `Quick (fail_dimension "0") ;
      test_case "1cm" `Quick (test_dimension (1.,`cm) "1cm") ;
      test_case "90deg" `Quick (test_dimension (90.,`deg) "90.0deg") ;
      test_case "100dpi" `Quick (test_dimension (100.,`dpi) "100.0dpi") ;
    ] ;
    "colors", [
      test_case "transparent" `Quick (test_color `Transparent "transparent");
      test_case "currentcolor" `Quick (test_color `Current_color " currentcolor");
      test_case "rgba red" `Quick (test_color (`Rgba (1.,0.,0.,1.)) " #ff0000") ;
      test_case "rgba red 2" `Quick (test_color (`Rgba (1.,0.,0.,1.)) " #ff0000ff") ;
      test_case "rgba red 3" `Quick (test_color (`Rgba (1.,0.,0.,1.)) " #f00") ;
      test_case "rgba green" `Quick (test_color (`Rgba (0.,1.,0.,1.)) " #0f0") ;
      test_case "rgba blue" `Quick (test_color (`Rgba (0.,0.,1.,1.)) " #00f") ;
      test_case "red" `Quick (test_color (`Named_color "red") " red") ;
    ] ;
    "axis-positions", [
      test_case "single" `Quick (test_axis_position (Single_kw `Right) "right") ;
      test_case "kw kw" `Quick (test_axis_position (XY (Kw `Center, Kw `Bottom)) "center bottom") ;
      test_case "kw value" `Quick (test_axis_position (XY (Kw `Center, Offset (`Length (10.,`px))))
       "center 10px") ;
      test_case "value value" `Quick (test_axis_position (XY (Offset (`Percent 5.),Offset (`Length (10.,`px))))
       "5% 10px") ;
      test_case "value kw" `Quick (test_axis_position (XY (Offset (`Length (10.,`px)), Kw `Top))
       "10px top") ;

    ];
  ]

let css = {|@namespace toto url("https://toto.org");
@charset "UTF-8";
@import "https://toto.org/style.css" layer(tutu.toto) screen and (orientation: landscape);
@layer toto, tutu.tata;
@layer toto {
   p { color: green ; }
   @layer tutu {
     h1 { color : yellow }
   }
   div {
     background: url("chess.png") 40% / 10em gray
     round fixed border-box;
     display: block flow list-item ;
   }
}
:root { --toto: red; }

iframe[seamless]:not(:hover) { color: blue; }
p,div[toto="tutu"]  { width: 1.px;
    align-content: unsafe center ;
    color: blue ;
    color: var(--toto, blue) ;
    background: no-repeat center / 80% url("../img/image.png");
}
div[class="foo"].bar[display="toto"][tutu="toto" i] ~ span > p { color: blue }
svg|h1::first-line,  h2#toto:hover {
    --toto: foo;
    width: 12em;
    accent-color: revert ;
    align-content: first baseline ;
    align-items: safe center ;
    align-self: flex-start;
    aspect-ratio: 1 / 2;
    background-attachment: local ;
    background-clip: padding-box ;
    background-color: white ;
    background-origin: content-box ;
    background-position: top 10px;
    background-image: linear-gradient(to bottom,
      rgba(255, 255, 0, 0.5),
      rgba(0, 0, 255, 0.5)
      ), url("catfront.png");
    background-repeat: space round ;
    background-size: auto 10%;
    background: local content-box, top local repeat-x url("https://foo.bar/image.png") blue;
    border-bottom-width: thin;
    border-bottom-style: dashed ;
    border-bottom-color: pink;
    border-bottom: red solid 1px ;
    border-color: red green blue ;
    border-width: 2px 1px;
    border-style: dashed ;
    color: red ;
    display: inline;
    flex-flow: column-reverse wrap-reverse;
    flex-grow: 10.;
    flex: auto;
    font-family: "Bou!", monospace ;
    font-size: 2em;
    font-variant: common-ligatures tabular-nums sub ordinal small-caps super;
    font-weight: 500 ;
    font-stretch: semi-condensed ;
    font: bold expanded  italic large / 2em serif;
    height: min-content ;
    width: fit-content(1em) ;
    justify-content: space-evenly ;
    justify-content: safe flex-start ;
    justify-items: legacy right ;
    justify-self: end ;
    justify-items: left legacy ;
    justify-items: safe self-start ;
    list-style: lower-roman url("../img/shape.png") outside;
    margin-top: 1em;
    margin: 1em;
    margin: -3px;
    /* top and bottom | left and right */
    margin: 5% auto;
    /* top | left and right | bottom */
    margin: 1em auto 2em;
    /* top | right | bottom | left */
    margin: 2px 1em 0 auto;
    max-height: 2em;
    max-width: fit-content(20em);
    min-width: 10%;
    padding: 10px 50px 20px;
  }
p { font: 12pt/14pt sans-serif }
p { font: 80% sans-serif }
p { font: x-large/110% "new century schoolbook", serif }
p { font: bold italic large Palatino, serif ;}
p { font: normal small-caps 120%/120% fantasy }
p { font: condensed oblique 12pt "Helvetica Neue", serif; }
.bar { color: pink;}
p small { color: #333;}
::-webkit-input-placeholder {
  color: #999999;
  background: #fff url('images/n2.gif') 0px 1px repeat-x;

}
|}

let test_css ?fname str =
  match Css.parse_string ?fname str with
  | exception (T.Error e) ->
    let msg = T.string_of_error e in
    Log.err (fun m -> m "%s" msg);
    exit 1
  | l ->
    let b = Buffer.create 256 in
    let fmt = Format.formatter_of_buffer b in
    Css.pp_string_css fmt l;
    Format.pp_print_flush fmt ();
    let str = Buffer.contents b in
    Log.info (fun m -> m "reparsing output");
    match Css.parse_string ?fname str with
    | exception (T.Error e) ->
       print_endline str ;
       let msg = T.string_of_error e in
       Log.err (fun m -> m "%s" msg);
       exit 1
    | css ->
       Buffer.reset b;
       Css.pp_string_css fmt l;
       Format.pp_print_flush fmt ();
       let str2 = Buffer.contents b in
       print_string str2;
       if str <> str2 then
         Logs.err (fun m -> m "outputs differ");
       let expanded = Css.expand_ns css in
       Css.pp_iri_css Format.std_formatter expanded

(*c==v=[File.string_of_file]=1.1====*)
let string_of_file name =
  let chanin = open_in_bin name in
  let len = 1024 in
  let s = Bytes.create len in
  let buf = Buffer.create len in
  let rec iter () =
    try
      let n = input chanin s 0 len in
      if n = 0 then
        ()
      else
        (
         Buffer.add_subbytes buf s 0 n;
         iter ()
        )
    with
      End_of_file -> ()
  in
  iter ();
  close_in chanin;
  Buffer.contents buf
(*/c==v=[File.string_of_file]=1.1====*)

let () =
  if Array.length Sys.argv > 1 then
    let fname = Sys.argv.(1) in
    let str = string_of_file fname in
    test_css ~fname str
  else ((*go() ;*) test_css css )
