(*********************************************************************************)
(*                OCaml-CSS                                                      *)
(*                                                                               *)
(*    Copyright (C) 2023 INRIA All rights reserved.                              *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** All case-sensitive keywords *)

type keyword = [
  | `Absolute
  | `All_petite_caps
  | `All_small_caps
  | `Auto
  | `Baseline
  | `Bold
  | `Bolder
  | `Border_box
  | `Block
  | `Bottom
  | `Break_spaces
  | `Caption
  | `Center
  | `Column
  | `Column_reverse
  | `Common_ligatures
  | `Condensed
  | `Contain
  | `Content
  | `Content_box
  | `Contents
  | `Contextual
  | `Collapse
  | `Cover
  | `Current_color
  | `Cursive
  | `Dashed
  | `Diagonal_frations
  | `Discretionary_ligatures
  | `Dotted | `Double
  | `Emoji
  | `End
  | `Expanded
  | `Extra_condensed
  | `Extra_expanded
  | `Fangsong
  | `Fantasy
  | `First | `Fixed | `Flex | `Flex_end | `Flex_start
  | `Flow | `Flow_root
  | `Full_width
  | `Grid
  | `Groove
  | `Hidden
  | `Historical_ligatures
  | `Icon
  | `Inherit
  | `Initial
  | `Inline | `Inline_block | `Inline_table
  | `Inline_flex | `Inline_grid
  | `Inset
  | `Inside
  | `Italic
  | `Jis78
  | `Jis83
  | `Jis90
  | `Jis04
  | `Justify
  | `Justify_all
  | `Large
  | `Larger
  | `Last
  | `Left
  | `Legacy
  | `Lighter
  | `Lining_nums
  | `List_item | `Local
  | `Match_parent
  | `Math
  | `Max_content
  | `Medium
  | `Menu
  | `Message_box
  | `Middle
  | `Min_content
  | `Monospace
  | `None
  | `Normal
  | `Nowrap
  | `No_common_ligatures
  | `No_contextual
  | `No_discretionary_ligatures
  | `No_historical_ligatures
  | `No_repeat
  | `Oldstyle_nums
  | `Ordinal
  | `Outset
  | `Outside
  | `Padding_box
  | `Petite_caps
  | `Pre
  | `Pre_line
  | `Pre_wrap
  | `Proportional_nums
  | `Proportional_width
  | `Relative
  | `Repeat_x
  | `Repeat_y
  | `Repeat
  | `Revert
  | `Revert_layer
  | `Right
  | `Row
  | `Row_reverse
  | `Ridge
  | `Round
  | `Run_in
  | `Ruby
  | `Ruby_base
  | `Ruby_base_container
  | `Ruby_text
  | `Ruby_text_container
  | `Safe
  | `Sans_serif
  | `Scroll
  | `Self_end
  | `Self_start
  | `Semi_condensed
  | `Semi_expanded
  | `Serif
  | `Simplified
  | `Slashed_zero
  | `Small
  | `Smaller
  | `Small_caps
  | `Small_caption
  | `Solid
  | `Space
  | `Space_around | `Space_between | `Space_evenly
  | `Stacked_fractions
  | `Start
  | `Static
  | `Status_bar
  | `Sticky
  | `Stretch
  | `Sub | `Super
  | `System_ui
  | `Table
  | `Table_row_group | `Table_header_group | `Table_footer_group
  | `Table_row | `Table_cell | `Table_column_group | `Table_column
  | `Table_caption
  | `Tabular_nums
  | `Text
  | `Text_bottom
  | `Text_top
  | `Thick
  | `Thin
  | `Titling_caps
  | `Top
  | `Ui_monospace
  | `Ui_rounded
  | `Ui_sans_serif
  | `Ui_serif
  | `Ultra_condensed
  | `Ultra_expanded
  | `Unicase
  | `Unicode
  | `Unsafe | `Unset
  | `Traditional
  | `Transparent
  | `Visible
  | `Wrap
  | `Wrap_reverse
  | `Xxx_large
  | `Xx_large
  | `Xx_small
  | `X_large
  | `X_small
]

let string_of_kw : [<keyword] -> string = function
| `Absolute -> "absolute"
| `All_petite_caps -> "all-petite-caps"
| `All_small_caps -> "all-small-caps"
| `Auto -> "auto"
| `Baseline -> "baseline"
| `Bold -> "bold"
| `Bolder -> "bolder"
| `Bottom -> "bottom"
| `Block -> "block"
| `Border_box -> "border-box"
| `Break_spaces -> "break-spaces"
| `Caption -> "caption"
| `Center -> "center"
| `Collapse -> "collapse"
| `Column -> "column"
| `Column_reverse -> "column-reverse"
| `Common_ligatures -> "common-ligatures"
| `Condensed -> "condensed"
| `Contain -> "contain"
| `Content -> "content"
| `Content_box -> "content-box"
| `Contents -> "contents"
| `Contextual -> "contextual"
| `Cover -> "cover"
| `Current_color -> "currentcolor"
| `Cursive -> "cursive"
| `Dashed -> "dashed"
| `Diagonal_frations -> "diagonal-fractions"
| `Discretionary_ligatures -> "discretionary-ligatures"
| `Dotted -> "dotted"
| `Double -> "double"
| `Emoji -> "emoji"
| `End -> "end"
| `Expanded -> "expanded"
| `Extra_condensed -> "extra-condensed"
| `Extra_expanded -> "extra-expanded"
| `Fantasy -> "fantasy"
| `Fangsong -> "fangsong"
| `First -> "first"
| `Fixed -> "fixed"
| `Flex -> "flex"
| `Flex_end -> "flex-end"
| `Flex_start -> "flex-start"
| `Flow -> "flow"
| `Flow_root -> "flow-root"
| `Full_width -> "full-width"
| `Grid -> "grid"
| `Groove -> "groove"
| `Hidden -> "hidden"
| `Historical_ligatures -> "historical-ligatures"
| `Icon -> "icon"
| `Inherit -> "inherit"
| `Initial -> "initial"
| `Inline -> "inline"
| `Inline_block -> "inline-block"
| `Inline_table -> "inline-table"
| `Inline_flex -> "inline-flex"
| `Inline_grid -> "inline-grid"
| `Inset -> "inset"
| `Inside -> "inside"
| `Italic -> "italic"
| `Jis78 -> "jis78"
| `Jis83 -> "jis83"
| `Jis90 -> "jis90"
| `Jis04 -> "jis04"
| `Justify -> "justify"
| `Justify_all -> "justify-all"
| `Large -> "large"
| `Larger -> "larger"
| `Last -> "last"
| `Left -> "left"
| `Legacy -> "legacy"
| `Lighter -> "lighter"
| `Lining_nums -> "lining-nums"
| `List_item -> "list-item"
| `Local -> "local"
| `Match_parent -> "match-parent"
| `Math -> "math"
| `Max_content -> "max-content"
| `Medium -> "medium"
| `Menu -> "menu"
| `Message_box  -> "message-box"
| `Middle -> "middle"
| `Min_content -> "min-content"
| `Monospace -> "monospace"
| `None -> "none"
| `Normal -> "normal"
| `Nowrap -> "nowrap"
| `No_common_ligatures -> "no-common-ligatures"
| `No_contextual -> "no-contextual"
| `No_discretionary_ligatures -> "no-discretionary-ligatures"
| `No_historical_ligatures -> "no-historical-ligatures"
| `No_repeat -> "no-repeat"
| `Oldstyle_nums -> "oldstyle-nums"
| `Ordinal -> "ordinal"
| `Outset -> "outset"
| `Outside -> "outside"
| `Padding_box -> "padding-box"
| `Petite_caps -> "petite-caps"
| `Pre -> "pre"
| `Pre_line -> "pre-line"
| `Pre_wrap -> "pre-wrap"
| `Proportional_nums -> "proportional-nums"
| `Proportional_width -> "proportional-width"
| `Relative -> "relative"
| `Repeat_x -> "repeat-x"
| `Repeat_y -> "repeat-y"
| `Repeat -> "repeat"
| `Revert -> "revert"
| `Revert_layer -> "revert-layer"
| `Ridge -> "ridge"
| `Right -> "right"
| `Round -> "round"
| `Row -> "row"
| `Row_reverse -> "row-reverse"
| `Ruby -> "ruby"
| `Ruby_base -> "ruby-base"
| `Ruby_base_container -> "ruby-base-container"
| `Ruby_text -> "ruby-text"
| `Ruby_text_container -> "ruby-text-container"
| `Run_in -> "run-in"
| `Safe -> "safe"
| `Sans_serif -> "sans-serif"
| `Scroll -> "scroll"
| `Self_end -> "self-end"
| `Self_start -> "self-start"
| `Semi_condensed -> "semi-condensed"
| `Semi_expanded -> "semi-expanded"
| `Serif -> "serif"
| `Simplified -> "simplified"
| `Slashed_zero -> "slashed-zero"
| `Small -> "small"
| `Smaller -> "smaller"
| `Small_caps -> "small-caps"
| `Small_caption -> "small-caption"
| `Solid -> "solid"
| `Space -> "space"
| `Space_around -> "space-around"
| `Space_between -> "space-between"
| `Space_evenly -> "space-evenly"
| `Stacked_fractions -> "stacked-fractions"
| `Static -> "static"
| `Start -> "start"
| `Status_bar -> "status-bar"
| `Sticky -> "sticky"
| `Stretch -> "stretch"
| `Sub -> "sub"
| `Super -> "super"
| `System_ui  -> "system-ui"
| `Table -> "table"
| `Table_row_group -> "table-row-group"
| `Table_header_group -> "table-header-group"
| `Table_footer_group -> "table-footer-group"
| `Table_row -> "table-row"
| `Table_cell -> "table-cell"
| `Table_column_group -> "table-column-group"
| `Table_column -> "table-column"
| `Table_caption -> "table-caption"
| `Tabular_nums -> "tabular-nums"
| `Text -> "text"
| `Text_bottom -> "text-bottom"
| `Text_top -> "text-top"
| `Thick -> "thick"
| `Thin -> "thin"
| `Titling_caps -> "titling-caps"
| `Top -> "top"
| `Traditional -> "traditional"
| `Transparent -> "transparent"
| `Ultra_condensed -> "ultra-condensed"
| `Ultra_expanded -> "ultra-expanded"
| `Unicase -> "unicase"
| `Unicode -> "unicode"
| `Unsafe -> "unsafe"
| `Unset -> "unsert"
| `Ui_monospace -> "ui-monospace"
| `Ui_rounded -> "ui-rounded"
| `Ui_sans_serif -> "ui-sans-serif"
| `Ui_serif -> "ui-serif"
| `Visible -> "visible"
| `Wrap -> "wrap"
| `Wrap_reverse -> "wrap-reverse"
| `Xxx_large -> "xxx-large"
| `Xx_large -> "xx-large"
| `Xx_small -> "xx-small"
| `X_large -> "x-large"
| `X_small -> "x-small"
