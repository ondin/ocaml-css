# OCaml-css -- CSS parser, printer and more

[🌐 OCaml-css homepage](https://zoggy.frama.io/ocaml-css/)

OCaml-css is an OCaml library to parse and print CSS. It can also
expand namespaces and perform computations on property values.

Parsing uses [Angstrom](https://github.com/inhabitedtype/angstrom/)
to dynamically choose the parser when encountering a property declaration.

Parser can be extended by defining additional properties.

## Documentation

Documentation is available [here](https://zoggy.frama.io/ocaml-css/refdoc/css/index.html).

## Development

Development is hosted on [Framagit](https://framagit.org/zoggy/ocaml-css).

There are already some [predefined properties](https://zoggy.frama.io/ocaml-css/refdoc/css/Css/P/index.html#predefined). Contributions are welcome to add more.
Shorthand properties are defined too.

CSS is released under GPL3 license.

## Installation

The `css` package is installable with opam:

```sh
opam install css
```

Current state of OCaml-css can be installed with:

```sh
$ opam pin add css git@framagit.org:zoggy/ocaml-css.git
```
